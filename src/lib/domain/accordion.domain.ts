import { AccordionDto } from "../../dto/accordion.dto";

export namespace AccordionDomain {
    export interface IAccordion extends AccordionDto.IAccordion {
        options: IAccordionOption[];
        rules: IAccordionRule[];
    }

    export interface IAccordionOption extends AccordionDto.IAccordionOption {
        uniqueKey: string;
    }

    export interface IAccordionRule extends AccordionDto.IAccordionRule {
        children: IAccordionRule[];
    }
}
