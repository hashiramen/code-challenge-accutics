/// <reference types="react-scripts" />

type ExtractKeys<T> = { [K in keyof T]: K }
