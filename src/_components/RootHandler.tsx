import * as React from 'react';
import { useStore } from '../store';
import Accordion from './Accordion';
import AccordionAddField from './AccordionAddField';
import AccordionOption from './AccordionOption';
import AccordionRule from './AccordionRule';

interface IRootHandler {}

const RootHandler: React.FunctionComponent<IRootHandler> = () => {
    const [state, dispatch] = useStore();

    const options = state.reduce(
        (prev, next) => [
            ...prev, {
                value: next.field_key,
                optionTitle: next.field_name
            }
        ], [] as React.ComponentProps<typeof AccordionRule>["fieldSelectOptions"]
    )

    return (
        <div className="wrapper padding box-shadow flex row">
            <ul style={{ flexGrow: 0, maxWidth: "75%", flexBasis: "75%" }}>
                {state.map(
                    (accordion) => (
                        <Accordion 
                            key={accordion.field_key}
                            title={accordion.field_name}
                            onTitleBlur={(event) => void dispatch({
                                type: "@FIELD_UPDATE",
                                payload: {
                                    field_key: accordion.field_key,
                                    key: "field_name",
                                    value: event.currentTarget.value,
                                }
                            })}
                        >
                            <ul>
                                {accordion.options.map(
                                    (option, subIndex) => (
                                        <AccordionOption 
                                            key={option.uniqueKey}
                                            data={option}
                                            dispatch={dispatch}
                                            fieldKey={accordion.field_key}
                                            pathIndex={[subIndex]}
                                        />
                                    )
                                )}
                                <li>
                                    <button
                                        onClick={() => void dispatch({
                                            type: "@OPTION_ADD",
                                            payload: {
                                                field_key: accordion.field_key,
                                            }
                                        })}
                                    >
                                        add option
                                    </button>
                                </li>
                            </ul>
                            <ul>
                                {accordion.rules.map(
                                    (rule, subIndex) => (
                                        <AccordionRule 
                                            key={subIndex}
                                            data={rule}
                                            fieldKey={accordion.field_key}
                                            pathIndex={[subIndex]}
                                            dispatch={dispatch}
                                            fieldSelectOptions={options}
                                        />
                                    )
                                )}
                                <li>
                                    <button
                                        onClick={() => void dispatch({
                                            type: "@RULE_ADD",
                                            payload: {
                                                field_key: accordion.field_key,
                                                pathIndex: []
                                            }
                                        })}
                                    >
                                        add group
                                    </button>
                                </li>
                            </ul>
                        </Accordion>
                    )
                )}
            </ul>
            <div style={{ flexGrow: 1, flexBasis: 0, maxWidth: "100%"}}>
                <AccordionAddField />
            </div>
        </div>
    );
};

export default RootHandler;
