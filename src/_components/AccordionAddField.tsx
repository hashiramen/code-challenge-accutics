import * as React from "react";
import { AccordionDomain } from "../lib/domain/accordion.domain";
import { useStore } from "../store";

interface IForm extends Pick<AccordionDomain.IAccordion, "field_name"> {}

const formKey: ExtractKeys<IForm> = {
    field_name: "field_name",
}


const AccordionAddField: React.FunctionComponent<unknown> = () => {
    const [, dispatch] = useStore();

    const [form, setForm] = React.useState<IForm>({
        field_name: ""
    });

    const handleChange = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => void setForm({
        ...form,
        [event.currentTarget.name]: event.currentTarget.value,
    }), [form])
    
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()

        dispatch({
            type: "@FIELD_ADD",
            payload: { ...form }
        })

        setForm({
            field_name: ""
        })
    }

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                name={formKey.field_name}
                placeholder={formKey.field_name}
                onChange={handleChange}
                value={form.field_name}
            />
            <input
                type="submit"
                value="Add Field"
            />
        </form>
    )
}

export default AccordionAddField;
