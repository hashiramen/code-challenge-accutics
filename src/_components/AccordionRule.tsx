import * as React from 'react';
import { AccordionDomain } from '../lib/domain/accordion.domain';
import { useStore } from '../store';

const ruleKey: ExtractKeys<Pick<AccordionDomain.IAccordionRule, "rule_field_key" | "rule_value">> = {
    rule_field_key: "rule_field_key",
    rule_value: "rule_value",
}

const inputArr = (arr: AccordionDomain.IAccordionRule[], config: Omit<IAccordionRuleProps, "data">) => arr.map(
    (rule, index) => (
        <AccordionRule
            key={index}
            data={rule}
            fieldKey={config.fieldKey}
            pathIndex={[...config.pathIndex, index]}
            dispatch={config.dispatch}
            fieldSelectOptions={config.fieldSelectOptions}
        />
    )
)


interface IAccordionRuleProps {
    pathIndex: number[];
    fieldKey: string;
    data: AccordionDomain.IAccordionRule;
    dispatch: ReturnType<typeof useStore>["1"];
    fieldSelectOptions: {
        value: string;
        optionTitle: string;
    }[];
    children?: never;
}

const AccordionRule: React.FunctionComponent<IAccordionRuleProps> = React.memo(({
    fieldKey,
    pathIndex, 
    data,
    dispatch,
    fieldSelectOptions,
}) => {
    const handleChange = React.useCallback((event: React.ChangeEvent<HTMLSelectElement> | React.FocusEvent<HTMLInputElement>) => void dispatch({
        type: "@RULE_UPDATE",
        payload: {
            field_key: fieldKey,
            pathIndex,
            key: event.currentTarget.name as keyof AccordionDomain.IAccordionRule,
            value: event.currentTarget.value,
        }
    }), [])

    const handleAdd = React.useCallback(() => void dispatch({
        type: "@RULE_ADD",
        payload: {
            field_key: fieldKey,
            pathIndex,
        }
    }), [])

    return (
        <li style={{ marginTop: ".4rem", backgroundColor: pathIndex.length % 2 === 0 ? "#31e42530" : "#00807a14"}}>
            <fieldset style={{borderColor: "transparent"}}>
                {pathIndex.length > 1 && <legend>and</legend>}
                <div>
                    <select
                        value={data.rule_field_key}
                        name={ruleKey.rule_field_key}
                        onChange={handleChange}
                    >
                        <option value="">
                            Select Field
                        </option>
                        {fieldSelectOptions.map(
                            field => (
                                <option
                                    key={field.value}
                                    value={field.value}
                                >
                                    {field.optionTitle}
                                </option>
                            )
                        )}
                    </select>
                    <input 
                        type="text" 
                        defaultValue={data.rule_value}
                        name={ruleKey.rule_value}
                        placeholder={ruleKey.rule_value}
                        onBlur={handleChange}
                    />
                </div>
                <ul>
                    {inputArr(data.children, {
                        fieldKey, 
                        pathIndex,
                        dispatch,
                        fieldSelectOptions,
                    })}
                </ul>
                <button
                    onClick={handleAdd}
                >
                    Add Group
                </button>
            </fieldset>
        </li>
    );
});

export default AccordionRule;
