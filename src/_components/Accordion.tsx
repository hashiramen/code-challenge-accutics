import * as React from 'react';

interface IAccordionProps {
    title: string;
    onTitleBlur?: React.InputHTMLAttributes<HTMLInputElement>["onBlur"];
}

const Accordion: React.FunctionComponent<IAccordionProps> = React.memo(({
    title,
    children,
    onTitleBlur,
}) => {
    const [expanded, setExpanded] = React.useState<boolean>(false);

    const [edit, setEdit] = React.useState<boolean>(false);

    const handleTitleChange = React.useCallback((event: React.FocusEvent<HTMLInputElement>) => {
        onTitleBlur && onTitleBlur(event)
        setEdit(false)
    }, [onTitleBlur])

    const handleToggleEdit = React.useCallback(() => void setEdit(!edit), [edit])

    const handleToggleExpand = React.useCallback(() => void setExpanded(!expanded), [expanded])

    return (
        <li className="border padding">
            <div className="flex space-between">
                <div>
                    {edit ? (
                        <input 
                            type="text"
                            defaultValue={title}
                            autoFocus
                            onBlur={handleTitleChange}
                        />
                    ) : (
                        <p>
                            {title}
                        </p>
                    )}
                </div>
                <div>
                    <button
                        onClick={handleToggleEdit}
                        disabled={edit}
                    >
                        edit
                    </button>
                    <button
                        onClick={handleToggleExpand}
                    >
                        {expanded ? "hide" : "expand"}
                    </button>
                </div>
            </div>
            <ul>
                {expanded && children}
            </ul>
        </li>
    );
});

export default Accordion;
