import * as React from "react";
import { AccordionDomain } from "../lib/domain/accordion.domain";
import { useStore } from "../store";

const optionKey: ExtractKeys<Pick<AccordionDomain.IAccordionOption, "option_label" | "option_value">> = {
    option_label: "option_label",
    option_value: "option_value",
}

interface IAccordionOptionProps {
    fieldKey: string;
    pathIndex: number[];
    data: AccordionDomain.IAccordionOption;
    dispatch: ReturnType<typeof useStore>["1"];
    children?: never;
}

const AccordionOption: React.FunctionComponent<IAccordionOptionProps> = ({
    data,
    dispatch,
    fieldKey,
    pathIndex,
}) => {
    const handleChange = React.useCallback((event: React.FocusEvent<HTMLInputElement>) => void dispatch({
        type: "@OPTION_UPDATE",
        payload: {
            field_key: fieldKey,
            pathIndex,
            key: event.currentTarget.name as keyof AccordionDomain.IAccordionOption,
            value: event.currentTarget.value,
        }
    }), [])

    const handleRemove = React.useCallback(() => void dispatch({
        type: "@OPTION_REMOVE",
        payload: {
            field_key: fieldKey,
            uniqueKey: data.uniqueKey,
        }
    }), [])

    return (
        <li className="border padding">
            <span>
                {pathIndex[0] + 1}. 
            </span>
            <input 
                type="text"
                defaultValue={data.option_label}
                name={optionKey.option_label}
                placeholder={optionKey.option_label}
                onBlur={handleChange}
            />
            <input 
                type="text"
                defaultValue={data.option_value}
                name={optionKey.option_value}
                placeholder={optionKey.option_value}
                onBlur={handleChange}
            />
            <button
                onClick={handleRemove}
            >
                X
            </button>
        </li>
    )
}

export default AccordionOption;
