import { IStoreState } from "."
import { AccordionDomain } from "../lib/domain/accordion.domain"
import { Action } from "./actions"

const updateElement = <T>(
    arr: T[],
    field: keyof T,
    keyCompareValue: any, 
    callback: (target: T) => T): T[] =>
        arr.map((target) => target[field] !== keyCompareValue
            ? target
            : callback(target))


const updateReccuringElement = <T>(
    { obj, pathIndex, childrenKey }: {
        obj: T | T[]
        pathIndex: number[]
        childrenKey?: keyof T
    },
    callback: (target: T) => T): T | T[] => {

        if ( Array.isArray(obj) ) return obj.map(
            (objItem, index) => index !== pathIndex[0]
                ? objItem
                : updateReccuringElement({
                        obj: objItem, 
                        pathIndex, 
                        childrenKey, 
                    },
                    callback
                ) as T
        )

        const isTargetedItem = pathIndex.length === 1

        if ( isTargetedItem ) return callback(obj)

        const deleveledPath =  pathIndex.slice(1)

        const children = childrenKey ? obj[childrenKey] : undefined;

        const updatedChildren = !children ? {} : {
            [childrenKey!]: updateReccuringElement({
                    obj: children as any,
                    pathIndex: deleveledPath,
                    childrenKey,
                },
                callback
            )
        }

        return {
            ...obj,
            ...updatedChildren
        }
    }

const reducer: (state: IStoreState, action: Action) => IStoreState = (state, action) => {
    switch (action.type) {
        case "@SET":
            return [ ...action.payload.data ]
        case "@FIELD_ADD":
            return [ ...state, {
                        field_key: (+new Date()).toString(),
                        field_name: action.payload.field_name,
                        options: [],
                        rules: [],
                    }]
        case "@FIELD_UPDATE":
                return [ ...updateElement(
                            state,
                            "field_key",
                            action.payload.field_key,
                            (targetAccordion) => ({
                                ...targetAccordion,
                                [action.payload.key]: action.payload.value,
                            })
                        )
                    ]
        case "@OPTION_ADD":
            return [ ...updateElement(
                    state,
                    "field_key",
                    action.payload.field_key,
                    (targetAccordion) => ({
                        ...targetAccordion,
                        options: [
                            ...targetAccordion.options, {
                                option_label: "",
                                option_value: "",
                                uniqueKey: (+new Date()).toString()
                            }
                        ]
                    })
                )]
        case "@OPTION_UPDATE":
            return [ ...updateElement(
                        state,
                        "field_key",
                        action.payload.field_key,
                        (targetAccordion) => ({
                            ...targetAccordion,
                            options: updateReccuringElement({
                                    obj: targetAccordion.options,
                                    pathIndex: action.payload.pathIndex,
                                },
                                (targetRule) => ({
                                    ...targetRule,
                                    [action.payload.key]: action.payload.value
                                })) as AccordionDomain.IAccordionOption[],
                        }))
                ]
        case "@OPTION_REMOVE":
            return [ ...updateElement(
                state,
                "field_key",
                action.payload.field_key,
                (targetAccordion) => ({
                    ...targetAccordion,
                    options: [ ...targetAccordion.options.filter(
                        (option) => option.uniqueKey !== action.payload.uniqueKey
                    )]
                })
            )]
        case "@RULE_ADD":
            const newRule: AccordionDomain.IAccordionRule = {
                rule_field_key: "",
                rule_value: "",
                children: []
            }
            return [ ...updateElement(
                        state, 
                        "field_key",
                        action.payload.field_key,
                        (targetAccordion) => ({
                            ...targetAccordion,
                            rules: !action.payload.pathIndex.length
                                ? [ ...targetAccordion.rules, newRule ]
                                : updateReccuringElement({
                                    obj: targetAccordion.rules,
                                    pathIndex: action.payload.pathIndex,
                                    childrenKey: "children",
                                },
                                (targetRule) => ({
                                    ...targetRule,
                                    children: [
                                        ...targetRule.children,
                                        newRule
                                    ]
                                })) as AccordionDomain.IAccordionRule[],
                        }))
                ]
        case "@RULE_UPDATE":
            return [ ...updateElement(
                        state,
                        "field_key",
                        action.payload.field_key,
                        (targetAccordion) => ({
                            ...targetAccordion,
                            rules: updateReccuringElement({
                                    obj: targetAccordion.rules,
                                    pathIndex: action.payload.pathIndex,
                                    childrenKey: "children",
                                },
                                (targetRule) => ({
                                    ...targetRule,
                                    [action.payload.key]: action.payload.value
                                })) as AccordionDomain.IAccordionRule[],
                        }))
                ]
    
        default: throw new Error();
    }
}

export default reducer;
