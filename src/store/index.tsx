import * as React from "react";
import { AccordionDto } from "../dto/accordion.dto";
import { AccordionDomain } from "../lib/domain/accordion.domain";
import { Action } from "./actions";
import reducer from "./reducer";

export type IStoreState = AccordionDomain.IAccordion[];

const StoreContext = React.createContext<[IStoreState, React.Dispatch<Action>]>([[], () => {}]);

const StoreProvider: React.FunctionComponent<unknown> = (props) => {
    const [state, dispatch] = React.useReducer<typeof reducer, IStoreState>(
        reducer,
        [],
        (state) => state
    );

    React.useEffect(() => {
        const data: AccordionDto.IAccordion[] = require("../data.json");

        const keyedData: AccordionDomain.IAccordion[] = data.map(
            (accordion) => ({
                ...accordion,
                options: [ ...accordion.options.map(
                    (option, index) => ({
                        ...option,
                        uniqueKey: `${index}_${+new Date()}`
                    } as AccordionDomain.IAccordionOption)
                )]
            })
        )

        dispatch({
            type: "@SET",
            payload: { data: keyedData }
        });
    }, [])

    return (
        <StoreContext.Provider 
            value={[state, dispatch]}
            {...props}
        />
    );
};

export default StoreProvider;


export const useStore = () => React.useContext(StoreContext);
