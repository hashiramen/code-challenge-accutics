import { AccordionDomain } from "../lib/domain/accordion.domain";

type ActionSet = { 
    type: "@SET";
    payload: {
        data: AccordionDomain.IAccordion[]
    };
}

type ActionFieldAdd = { 
    type: "@FIELD_ADD";
    payload: Required<Pick<AccordionDomain.IAccordion, "field_name">>
}

type ActionFieldUpdate = { 
    type: "@FIELD_UPDATE";
    payload: Required<Pick<AccordionDomain.IAccordion, "field_key">> & {
        key: keyof AccordionDomain.IAccordion;
        value: any;
    }
}

type ActionOptionAdd = {
    type: "@OPTION_ADD",
    payload: {
        field_key: string;
    }
}

type ActionOptionUpdate = {
    type: "@OPTION_UPDATE",
    payload: Required<Pick<AccordionDomain.IAccordion, "field_key">> & {
        pathIndex: number[];
        key: keyof AccordionDomain.IAccordionOption;
        value: any;
    }
}

type ActionOptionRemove = {
    type: "@OPTION_REMOVE",
    payload: Required<Pick<AccordionDomain.IAccordion, "field_key">>
        & Required<Pick<AccordionDomain.IAccordionOption, "uniqueKey">>
}

type ActionRuleAdd = {
    type: "@RULE_ADD",
    payload: {
        field_key: string;
        pathIndex: number[];
    }
}

type ActionRuleUpdate = {
    type: "@RULE_UPDATE",
    payload: Required<Pick<AccordionDomain.IAccordion, "field_key">> & {
        pathIndex: number[];
        key: keyof AccordionDomain.IAccordionRule;
        value: any;
    }
}

export type Action = 
    | ActionSet
    | ActionFieldAdd
    | ActionFieldUpdate
    | ActionOptionUpdate
    | ActionOptionAdd
    | ActionOptionRemove
    | ActionRuleAdd
    | ActionRuleUpdate
