export namespace AccordionDto {
    export interface IAccordion {
        field_key: string;
        field_name: string;
        options: IAccordionOption[];
        rules: IAccordionRule[];
    }

    export interface IAccordionOption {
        option_label: string;
        option_value: string;
    }

    export interface IAccordionRule {
        rule_field_key: string;
        rule_value: string;
        children: IAccordionRule[];
    }
}